export function fight(firstFighter, secondFighter) {
  let firstHealth = firstFighter.health;
  let secondHealth = secondFighter.health;
  while (true) {
    secondHealth -= getDamage(firstFighter, secondFighter);
    if (secondHealth <= 0) {
	  const winner = { ...firstfighter };
	  winner.health = firsthealth;
      return winner;
    }
    firstHealth -= getDamage(secondFighter, firstFighter);
    if (firstHealth <= 0) {
	  const winner = { ...secondFighter };
	  winner.health = secondHealth;
	  return winner;
    }
  }
}

export function getDamage(attacker, enemy) {
  return getHitPower(attacker) - getBlockPower(enemy);
}

export function getHitPower(fighter) {
  return fighter.attack * getСoefficient();
}
export function getBlockPower(fighter) {
  return fighter.defense * getСoefficient();
}
export function getСoefficient() {
	return 1+ Math.random();
}
