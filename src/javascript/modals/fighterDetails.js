import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';

export  function showFighterDetailsModal(fighter) {
  const title = 'Fighter info';
  const bodyElement = createFighterDetails(fighter);
  showModal({ title, bodyElement });
}

function createFighterDetails(fighter) {
  const { name } = fighter;

  const fighterDetails = createElement({ tagName: 'div', className: 'modal-body' });
  const nameElement = createElement({ tagName: 'span', className: 'fighter-name' });

  const detailsText = createElement({ tagName: 'p'});
  detailsText.innerHTML = `<ul>
      <li>attack: ${fighter.attack}</li>
      <li>defense: ${fighter.defense}</li>
      <li>health: ${fighter.health}</li></ul>`;

  const fighterImage = createElement({ tagName: 'img', className: 'fighter-image', attributes: {src: fighter.source}});
  nameElement.innerText = name;

  fighterDetails.append(nameElement);
  fighterDetails.append(detailsText);
  fighterDetails.append(fighterImage);

  return fighterDetails;
}
