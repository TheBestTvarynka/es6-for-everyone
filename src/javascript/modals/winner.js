import { showModal } from "./modal";
import {createElement} from "../helpers/domHelper";

export function showWinnerModal(fighter) {
  const title = 'Winner';
  const bodyElement = createWinnerDetails(fighter);
  showModal({ title, bodyElement });
}

function createWinnerDetails(fighter) {
  const { name } = fighter;

  const winnerDetails = createElement({ tagName: 'div', className: 'modal-body' });
  const winnerName = createElement({ tagName: 'span', className: 'fighter-name' });
  const winnerHealth = createElement({ tagName: 'p' });
  winnerHealth.innerHTML = `Heals: ${fighter.health.toFixed(2)}`;
  const winnerImage = createElement({ tagName: 'img', className: 'fighter-image', attributes: {src: fighter.source}});

  winnerName.innerText = name;
  winnerDetails.append(winnerName);
  winnerDetails.append(winnerHealth);
  winnerDetails.append(winnerImage);

  return winnerDetails;
}
